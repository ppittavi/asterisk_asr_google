#!/usr/bin/env python3
import sys
from os import path, remove
from time import sleep

import soundfile as sf
import speech_recognition as sr
from asterisk.agi import AGI

agi = AGI()
callerId = agi.env["agi_callerid"]
uniqueId = agi.env["agi_uniqueid"]
lang = agi.env["agi_language"]

temp_file = f"/tmp/{uniqueId}_{callerId}"
transcoded_file = f"{temp_file}.flac"

agi.verbose(f"Starting Google Cloud Speech to Text for {uniqueId}  on language {lang}")
agi.verbose(f"call from {callerId}")

temp_file = f"/tmp/{uniqueId}_{callerId}"
agi.record_file(temp_file, silence="2", escape_digits="#")

# waiting for recorded file
while not path.exists(f"{temp_file}.gsm"):
    agi.verbose(f"waiting recorded file  for {uniqueId}")
    sleep(1)

agi.verbose(f"Transcoding audio in FLAC  for {uniqueId}")

data, samplerate = sf.read(f"{temp_file}.gsm")
sf.write(transcoded_file, data, samplerate)


# use the audio file as the audio source
r = sr.Recognizer()
with sr.AudioFile(transcoded_file) as source:
    audio = r.record(source)  # read the entire audio file

# recognize speech using Google Cloud Speech
try:
    recognized_text = r.recognize_google_cloud(audio, language=lang)
    agi.verbose(
        f'Google Cloud Speech thinks you said in call {uniqueId}: "{recognized_text}"'
    )

except sr.UnknownValueError:
    agi.verbose(f"Google Cloud Speech could not understand audio in call {uniqueId} ")
    sys.exit(1)
except sr.RequestError as e:
    agi.verbose(
        f"Could not request results from Google Cloud Speech service in call {uniqueId}:  {e}"
    )
    sys.exit(1)
except Exception as e:
    agi.verbose(f"{type(e).__name__}: {e}")
    sys.exit(1)

agi.set_variable("recognized_text", recognized_text)

# remove files
agi.verbose(f"Removing speech to text files for {uniqueId}")

for to_be_removed in [f"{temp_file}.gsm", transcoded_file]:
    remove(to_be_removed)
