Asterisk Google Cloud ASR-TTS
=============================

Based on Andrius Asterisk docker image:
 https://hub.docker.com/r/andrius/asterisk/

In this project I use [
Google Cloud Platform (GCP)](https://cloud.google.com)  [Speech-to-Text ](https://cloud.google.com/speech-to-text) API  to create an asterisk reusable   automatic speech recognition (ASR) and  [Text-to-Speech](https://cloud.google.com/text-to-speech) (TTS)  API to play it back.

It could be used in any language [supported by google speech to text](https://cloud.google.com/speech-to-text/docs/languages)

Simple agi scrips to do it are written using Python3

## Getting started

### Clone project
```bash
git clone https://gitlab.com/ppittavi/asterisk_asr_google.git

cd asterisk_asr_google
```
### Create your google credential json file
Follow [this guide](https://cloud.google.com/docs/authentication/getting-started) and download your json in conf dir with name ```conf/google_credential.json```

### Set your machine ip
In Docker with bridge network mode Asterisk works behind a NAT.

Edit ```conf/pjsip.conf``` file and change this two lines with your machine ip:

```
external_media_address=<PUT HERE YOUR MACHINE IP>
external_signaling_address=<PUT HERE YOUR MACHINE IP>
```
### Build

```bash
docker-compose build
```
It could take some time to build

### Start

```bash
docker-compose up -d
```


## Usage

###  Register your phone

I use a real ip phone  for my test but you could use a softphone as well, settings:
```
Authentication User: 123
Authentication Password: 123
SIP Server Address:<PUT HERE YOUR MACHINE IP>
```


###  Test Asterisk
Call ```111``` and you should listen a message, or  call ```222``` for echo test.

### Try it in english

Call ```444```  to start a Parrot Service. What you say in english after the beep is played back to you with automatic voice.


### Try it in italian

Call ```333```  to start a Parrot Service in Italian


## How it works

As you can see in dialplan (```conf/extensions.conf```) language variable is set in the channel, so you  can create the same  service  in other languages [supported by google speech to text](https://cloud.google.com/speech-to-text/docs/languages) simply changing this value.

ex:
```
exten => 333,1,Answer()
same => n,Set(CHANNEL(language)=it)
...

exten => 444,1,Answer()
same => n,Set(CHANNEL(language)=en)
...
```

In speech to text a temp file is recorded and then sent to GCP, best result is saved in a ```recognized_text``` variable reusable in channel.
ex:
```
...
same => n,AGI(/var/lib/asterisk/agi-bin/speech_to_text.py)
same => n,NoOp(Returned text for call ${agi_uniqueid}: ${recognized_text} )
...
```


For text to speech you can pass any string to agi script and it will be played back.
ex:
```
...
same => n,AGI(/var/lib/asterisk/agi-bin/text_to_speech.py, "Text who should be sent to channel")
...
```
## Links

Some links of tutorials I used in this project:

- https://zaf.github.io/asterisk-speech-recog/
- https://deividsdocs.com/2018/08/01/speech-recognition-with-google-cloud-api-on-asterisk-15-and-centos/
- https://realpython.com/python-speech-recognition/
- https://codelabs.developers.google.com/codelabs/cloud-speech-text-python3#6
- https://pypi.org/project/SoundFile/
